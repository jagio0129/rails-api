# frozen_string_literal: true

require 'faker'

class InitialDatabase
  # データをすべて削除してインクリメントをリセット
  def reset_models
    p 'reset databases'
    models.map(&:singularize).map(&:camelize).each do |model|
      eval("#{model}.delete_all")
      eval("#{model}.reset_pk_sequence")  # initilize primary_id
    end
  end

  private
  def models
    ActiveRecord::Base.connection.tables.reject do |model|
      # 元からあるテーブルは除外
      default_tables.include?(model)
    end
  end

  def default_tables
    ["schema_migrations", "ar_internal_metadata"]
  end
end

class SeedModels
  
end

InitialDatabase.new.reset_models
